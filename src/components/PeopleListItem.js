import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';

import {capitalizeFirstLetter} from '../util'
const PeopleListItem = props =>{
    const {people} = props;
    const {title, first, last} = people.name;

    return (
        <TouchableOpacity>
            <View style = {styles.line}>
                <Image style ={styles.avatar} source= {{uri: people.picture.thumbnail }} />
                <Text style = {styles.lineText}>
                    {`${
                        capitalizeFirstLetter(title)
                    } ${
                        capitalizeFirstLetter(first)
                    } ${
                        capitalizeFirstLetter(last)
                        }`}
                </Text>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create ({
    line: {
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#475959',

        alignItems: 'center',
        flexDirection: 'row',

    },
    lineText:{
        fontSize: 20,
        paddingLeft: 15,
        flex: 8,
        
    },
    avatar: {
        aspectRatio: 1,
        flex: 1,
        marginLeft: 15,
        borderRadius: 45,
           
    }

});

export default PeopleListItem;