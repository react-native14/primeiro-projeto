/* 
Modifica a primeira letra de uma palavra para maiuscula
Exemplo = cachorro -> Cachorro 
*/

const capitalizeFirstLetter = string => {
    return string[0].toUpperCase() + string.slice(1)
}

export default capitalizeFirstLetter;